# [tkani.net](https://tkani.net) source codes

<br/>

### Run tkani.net on localhost

    # vi /etc/systemd/system/tkani.net.service

Insert code from tkani.net.service

    # systemctl enable tkani.net.service
    # systemctl start tkani.net.service
    # systemctl status tkani.net.service

http://localhost:4052
